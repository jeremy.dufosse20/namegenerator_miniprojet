"""
    Ce programme affiche un nom fictif d'un utilisateur à partir de son nom, de son prénom et d'un univers fictif.
"""
from Univers import *
from tkinter import *


def call_gen_name():
    """
        Fait appel aux différentes fonctions "choix_univers" génération de nom et l'affichage du nom généré
    """
    if (univers.get() != "Univers"):

        nom_univers, prenom_univers = choix_univers()

        nom_genere=randomgen_nom(nom_univers)
        prenom_genere = call_gen_name_prenom(prenom_univers)

        nom_gen.set("%s %s" % (nom_genere, prenom_genere))

    else:
        nom_gen.set("Pas d'univers selectionne")


def call_gen_name_prenom(prenom_univers):
    """
    Permet de générer un prénom en fonction de l'univers selectionné

    Paramètre
    ----------
    Le prénom de l'univers choisi

    Retourne
    -------
    Le prenom généré
    """
    prenom = Prenom.get()
    prenom_genere = wordlist[prenom_univers][prenom[2].upper()]
    return prenom_genere


def randomgen_nom(nom_univers):
    """
    Permet de générer un nom en fonction de l'univers selectionné

    Paramètre
    ----------
    Le nom de l'univers choisi

    Retourne
    -------
    Le nom généré
    """
    nom = Nom.get()
    nom_genere = wordlist[nom_univers][nom[0].upper()]
    return nom_genere


def choix_univers():
    """
    Permet de définir l'univers choisi

    Retourne
    -------
    Le nom et le prénom de l'univers
    """
    nom_univers = "%s_nom" % (univers.get())
    prenom_univers = "%s_prenom" % (univers.get())
    return nom_univers,prenom_univers


#Partie IHM

Affichage = Tk()
Affichage.title("Name Generator")

Prenom = StringVar()
Prenom.set('Votre prenom')

Nom = StringVar()
Nom.set('Votre nom')

nom_gen = StringVar()
nom_gen.set(' ')

menu_univers = ('game_of_thrones', 'star_wars', 'warcraft')
univers = StringVar()
univers.set("Univers")


input_Nom = Entry(Affichage, textvariable=Nom)
input_Nom.grid(column=2, row=3, padx=5,pady=10, ipady=10)


input_Prenom = Entry(Affichage, textvariable=Prenom)
input_Prenom.grid(column=3, row=3, padx=5,pady=10, ipady=10)


button_Chiffrer = Button(Affichage, text="Quel est votre nom ?", command=call_gen_name)
button_Chiffrer.grid(row=4, column=3, padx=15,pady=30, ipady=10)


select_universe = OptionMenu(Affichage, univers, *menu_univers)
select_universe.grid(row=4, column = 2, padx=15,pady=5, ipady=10)


lab_res = Label(Affichage, textvariable=nom_gen)
lab_res.grid(column=2, row=7, padx=5,pady=5, ipady=5)

Affichage.geometry("320x230")

Affichage.mainloop()