import unittest
from NameGenerator import *
from Univers import *


class TestNameGenerator(unittest.TestCase):


    def test_choix_univer(self):
        self.assertEqual(choix_univers(), ("star_wars_nom","star_wars_prenom"))

    def test_transformation_ASCII(self):
        #testé avec nom: Dufossé prenom: Jérémy univers: star wars
        self.assertEqual(randomgen_nom("star_wars_nom"),("Padawan"))
        self.assertEqual(randomgen_nom("star_wars_prenom"), ("Solo"))
